#!/bin/bash

set -e
set -u

CODE="${PWD}"

function link() {
    echo "linking ${HOME}/${1} -> ${CODE}/${1}"

    # Remove existing - only works if it's a symlink
    rm -f "${HOME}/${1}"

    ln -s "${CODE}/${1}" "${HOME}/${1}"
}

link .local/share/kupfer
link .config/kupfer
link .config/nvim
link .config/ripgreprc

echo 'echo \'export RIPGREP_CONFIG_PATH="$HOME/.config/ripgreprc"\' >> ~/.bashrc"
