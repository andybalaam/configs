__kupfer_name__ = _("MATE Session Management")
__kupfer_sources__ = ("GnomeItemsSource", )
__description__ = _("Special items and actions for MATE environment")
__version__ = "2012-10-16"
__author__ = "Ulrik Sverdrup <ulrik.sverdrup@gmail.com>"

"""
Changes:
    2022-09-27 Andy Balaam
        + Port to MATE
    2012-10-16 Karol Będkowski:
        + support Gnome3; closes lp#788713;
          author: Joseph Lansdowne
"""

from kupfer.plugin import session_support as support


# sequences of argument lists
LOGOUT_CMD = (["mate-session-save", "--logout-dialog"],)
SHUTDOWN_CMD = (["mate-session-save", "--shutdown-dialog"],)
LOCKSCREEN_CMD = (["mate-screensaver-command", "--lock"],)

class GnomeItemsSource (support.CommonSource):
    def __init__(self):
        support.CommonSource.__init__(self, _("MATE Session Management"))
    def get_items(self):
        return (
            support.Logout(LOGOUT_CMD),
            support.LockScreen(LOCKSCREEN_CMD),
            support.Shutdown(SHUTDOWN_CMD),
        )

