call plug#begin()
    Plug 'neovim/nvim-lspconfig'
    Plug 'simrat39/rust-tools.nvim'
call plug#end()


" Format rust code on save
let g:rustfmt_autosave = 1



lua <<EOF
local opts = {
    tools = {
        autoSetHints = true,
        inlay_hints = {
            only_current_line = true,
            show_parameter_hints = true,
        },
    }
}

require('rust-tools').setup(opts)
EOF
