" ### Fundamentals

" Requires:
" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
" nvm install 14

call plug#begin()
    Plug 'chriskempson/base16-vim'
    Plug 'editorconfig/editorconfig-vim'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'romainl/vim-qf'
    Plug 'scrooloose/nerdtree'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-surround'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'yegappan/mru'
    Plug 'leafOfTree/vim-svelte-plugin'
call plug#end()

filetype plugin on

" No tabs except in Makefiles
set expandtab
set tabstop=4
set shiftwidth=4
set tw=80

" Store backups and swap files in a special dir.
set directory=~/.vimdirectory//
set backupdir=~/.vimbackup/
set undodir=~/.vimundo/

" Save undo history
set undofile

" Do syntax highlighting even if it is slow
set redrawtime=10000

" Ensure :find looks inside directories in pwd
set path+=**

" ### Automation
" Reload vimrc on change
augroup reload_vimrc
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END
" Makefiles use tabs
augroup tabs_in_make
    autocmd!
    autocmd FileType make setlocal noexpandtab
augroup END

" ### Appearance

" Don't highlight TODO
highlight Todo ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE

" Line numbers
set number
highlight LineNr
    \ term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE
    \ gui=NONE guifg=DarkGrey guibg=NONE

" Column width of 80
set colorcolumn=80
highlight ColorColumn ctermbg=darkgrey guibg=darkgrey

" Make bracket matching (more subtle)
augroup tabs_in_make
    autocmd!
    autocmd ColorScheme * highlight MatchParen cterm=none ctermbg=none ctermfg=green
augroup END

" Colours
set background=dark

let base16colorspace=256
colorscheme base16-irblack
let g:airline_theme='base16_irblack'
" NOTE: this needs:
"    cd ~/code/others
"    git clone https://github.com/chriskempson/base16-shell.git
"    echo 'source ~/code/others/base16-shell/scripts/base16-irblack.sh' >> ~/.bashrc

" Fix the fact that my menu items were not being highlighted in Coc
:hi CocMenuSel ctermbg=19

" ### Search
" Esc to remove highlighting
nnoremap <silent> <esc> :noh<cr><esc>
" Search with ripgrep
set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case\ --glob=!.git\ --hidden
set grepformat=%f:%l:%c:%m
" F3 for nice search (also depends on vim-qf to jump to quicklist)
noremap <F3> :silent grep! 

" Highlight trailing white space
highlight ExtraWhitespace cterm=undercurl ctermfg=green guifg=green
match ExtraWhitespace /\s\+$/

" ### Status line
" Uses vim-airline, theme in Colours section
let g:airline_symbols_ascii = 1
let g:airline_section_a = ""
let g:airline_section_b = ""
let g:airline_section_x = ""
let g:airline_section_y = ""
let g:airline_section_z = "%c"

" ### Svelte
let g:vim_svelte_plugin_load_full_syntax = 1
let g:vim_svelte_plugin_use_typescript = 1

" ### Keyboard shortcuts

" Do what I meant when I type a leading capital
command! W :w
command! Qa :qa

" Split and navigate windows easily
noremap <leader><Left> :vsplit<CR>
noremap <leader><Right> :vsplit<CR><C-w><Right>
noremap <leader><Up> :split<CR>
noremap <leader><Down> :split<CR><C-w><Down>
noremap <A-Left> <C-w><Left>
noremap <A-Right> <C-w><Right>
noremap <A-Up> <C-w><Up>
noremap <A-Down> <C-w><Down>

" Navigate quicklist
noremap <C-Down> :cnext<CR>
noremap <C-Up> :cprevious<CR>
noremap <C-Left> :cclose<CR>
nmap <C-Right> <Plug>(qf_qf_toggle)

" Open/focus NerdTree with F5
noremap <F4> :NERDTreeFind<CR>:wincmd p<CR>
noremap <F5> :NERDTreeFocus<CR>:NERDTreeRefreshRoot<CR>

noremap <F6> :set colorcolumn=80<CR>
noremap <F7> :set colorcolumn=100<CR>
noremap <F8> :set colorcolumn=120<CR>

" ### File browser
let NERDTreeWinSize = 20

" ### Language support

" tab-completion works like bash
set wildmode=longest:full,full
set wildmenu

" Format TypeScript etc on save in one project only
" Note: rust is done in coc-settings.json
autocmd BufWritePre **/matrix-wysiwyg/platforms/web/src/** :call CocAction('format')
autocmd BufWritePre **/matrix-wysiwyg/platforms/web/lib/** :call CocAction('format')
autocmd BufWritePre **/matrix-wysiwyg/platforms/web/*.ts :call CocAction('format')
autocmd BufWritePre **/matrix-wysiwyg/platforms/web/*.json :call CocAction('format')
autocmd BufWritePre **/matrix-wysiwyg/platforms/web/*.md :call CocAction('format')

" Coc
" :PlugUpgrade
" :PlugUpdate
" :CocInstall coc-json
" :CocInstall coc-rust-analyzer
" :CocInstall coc-tslint
" :CocInstall coc-tsserver
" :CocInstall coc-prettier
" :CocInstall coc-git
" Also see coc-settings.json

set hidden
set nobackup
set nowritebackup
set updatetime=300
set shortmess+=c
set signcolumn=number

highlight CocHintSign ctermfg=darkgrey guifg=darkgrey

nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

autocmd CursorHold * silent call CocActionAsync('highlight')

xmap <leader>a  <Plug>(coc-codeaction-selected)  " e.g. \aw to fix this word
nmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>rn <Plug>(coc-rename)
nmap <leader>ac  <Plug>(coc-codeaction)
nmap <leader>cl  <Plug>(coc-codelens-action)

nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"

nnoremap <silent><nowait> <leader>as  :<C-u>CocList diagnostics<cr>

command! -nargs=0 Format :call CocActionAsync('format')
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

inoremap <silent><expr> <c-space> coc#refresh()
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "<CR>"
